#!/usr/bin/env python
"""Test suite including decorators for the Simple Socket Server application."""
__docformat__ = "restructuredtext"

import logging

from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester import failUnlessEqual

from server_suite_test_base import ServerSuiteTestBase

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )


class ExampleProjectTestSuiteDecorators1(ServerSuiteTestBase):
    """Tests demonstrating the use of decorators for Simple Server GET
    commands.
    """

    def __init__(self):
        """Initialise variables used in the tests."""
        ServerSuiteTestBase.__init__(self)
        self.logger = logging.getLogger('ExampleTestSuite')

    @test(testID="test-get-decorator-01",
          approx_exec_time=10)
    def test_get_decorator_01(self):
        """Send GET command and expect GET response, approx_exec_time=10.

        In order to run this test, the command line option --max_time must
        be used, with a value >= 10 eg --max_time=20.
        """
        message = 'GET data'
        message_type = 'GET message'
        self.send_message(message, 'GET message')

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)

    @test(testID="test-get-decorator-02",
          testFatal="Put the reason why the test dies in this comment")
    def test_get_decorator_02(self):
        """Send GET command and expect GET response, testFatal decorator.

        In order to run this test, the command line option --only-fatal must
        be used, with a value >= 10 eg --max_time=20.
        """
        message = 'GET data'
        message_type = 'GET message'
        self.send_message(message, 'GET message')

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)

    @test(testID="test-get-decorator-03",
          testFails="Put the reason why the test fails in this comment")
    def test_get_decorator_03(self):
        """Send GET command and expect GET response, testFails decorator.

        In order to run this test, the command line option --only-failing must
        be used, with a value >= 10 eg --max_time=20.
        """
        message = 'GET data'
        message_type = 'GET message'
        self.send_message(message, 'GET message')

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)

    @test(testID="test-get-decorator-04",
          todo="Add a comment")
    def test_get_decorator_04(self):
        """Send GET command and expect GET response, todo decorator.

        In order to run this test, the command line option --only-todo must
        be used.
        """
        message = 'GET data'
        message_type = 'GET message'
        self.send_message(message, 'GET message')

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)


class ExampleProjectTestSuiteDecorators2(ServerSuiteTestBase):
    """Tests demonstrating the use of decorators for Simple Server PUT
    commands.
    """

    def __init__(self):
        """Initialise variables used in the tests."""
        ServerSuiteTestBase.__init__(self)
        self.logger = logging.getLogger('ExampleTestSuite')

    @test(testID="test-put-decorator-01")
    def test_put_decorator_info(self):
        """Send PUT command and expect PUT response."""
        message = 'PUT data'
        message_type = 'PUT message'
        self.send_message(message, message_type)

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)

if __name__ == "__main__":
    runModule()
