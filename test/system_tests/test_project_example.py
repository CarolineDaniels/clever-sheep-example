#!/usr/bin/env python
"""Test suite for the Simple Socket Server application."""
__docformat__ = "restructuredtext"

import logging

from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester import failUnlessEqual

from server_suite_test_base import ServerSuiteTestBase

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )

class ExampleProjectTestSuite1(ServerSuiteTestBase):
    """Tests for Simple Server GET commands."""

    def __init__(self):
        """Initialise variables used in the tests."""
        ServerSuiteTestBase.__init__(self)
        self.logger = logging.getLogger('ExampleTestSuite')

    @test(testID="test-get-01")
    def test_get_info(self):
        """Send GET command and expect GET response."""
        message = 'GET data'
        message_type = 'GET message'
        self.send_message(message, 'GET message')

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)

class ExampleProjectTestSuite2(ServerSuiteTestBase):
    """Tests for Simple Server PUT commands."""

    def __init__(self):
        """Initialise variables used in the tests."""
        ServerSuiteTestBase.__init__(self)
        self.logger = logging.getLogger('ExampleTestSuite')

    @test(testID="test-put-01")
    def test_put_info(self):
        """Send PUT command and expect PUT response."""
        message = 'PUT data'
        message_type = 'PUT message'
        self.send_message(message, message_type)

        response = self.wait_for_event(1.0, type=message_type)

        failUnlessEqual(message, response.message)

if __name__ == "__main__":
    runModule()
