#!/usr/bin/env python
"""Base class."""
__docformat__ = "restructuredtext"

import logging
import socket
import errno

from CleverSheep.Test.TestEventStore import TestEvent
from CleverSheep.Test.TestEventStore import eventStore

from project_test_base import ProjectTestBase


class ServerSuiteTestBase(ProjectTestBase):
    """This class is a base class for all the tests within a subset of this
    project.

    Helper functions that are useful fora single test suites are placed here.
    """

    def __init__(self):
        """Initialise variables used in the tests."""
        self.logger = logging.getLogger('ServerSuiteTestBase')
        self.server = {}
        self.socket = None
        ProjectTestBase.__init__(self)

    def suiteSetUp(self):
        """Set up carried out once before each Suite of tests."""
        self.server = {'address': '127.0.0.1',
                       'port': 20009}
        # Connect to the server
        self.logger.debug('creating socket')
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger.debug('connecting to server')
        self.socket.connect((self.server['address'], self.server['port']))

    def suiteTearDown(self):
        """Tear down carried out once at the end of a Suite of tests."""
        self.socket.shutdown()
        self.socket.close()

    def send_message(self, message, message_type):
        """Sends a message to the server socket"""
        self.logger.debug('sending data: "%s, type = %s"',
                          message, message_type)
        len_sent = self.socket.send(message)

        # Receive a response
        self.logger.debug('waiting for response')
        response = self.receive_message()
        self.logger.debug('response from server: "%s"', response)
        # Store the response
        eventStore.addEvent(TestEvent(message=response, type=message_type))

    def receive_message(self):
        """Reads input from the socket."""
        message = ''
        while True:
            try:
                data = self.socket.recv(1024)
                message += data
            except IOError, exc:
                if exc.errno == errno.EAGAIN:
                    # No more data to read.
                    return message
            if len(data) != 1024:
                return message

            if len(data) == 0:
                # The socket has closed
                return message

