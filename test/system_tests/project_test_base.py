#!/usr/bin/env python
"""This module contains a base class for all tests within this project"""
__docformat__ = "restructuredtext"

import logging
import sys

from CleverSheep.Test import Tester
from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.TestEventStore import eventStore, TestEvent

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )

# Options used in all tests
Tester.add_option("--max-time",
                  action="store",
                  type="int",
                  metavar="MAX",
                  help="Limit test selection to tests that take no more than"
                       "MAX seconds.")

Tester.add_option("--ignore-failing",
                  action="store_true",
                  help="Ignore tests that are marked as expected fails")

Tester.add_option("--only-failing",
                  action="store_true",
                  help="Ignore tests that are marked as expected fails")

Tester.add_option("--ignore-fatal",
                  action="store_true",
                  help="Ignore tests that are marked as expected fails")

Tester.add_option("--only-fatal",
                  action="store_true",
                  help="Ignore tests that are marked as expected fails")

Tester.add_option("--ignore-todo",
                  action="store_true",
                  help="Ignore tests that are marked as expected fails")

Tester.add_option("--only-todo",
                  action="store_true",
                  help="Ignore tests that are marked as expected fails")

Tester.add_option("--abandon-suite-on-bad-setup",
                  action="store_true",
                  help='Skip rest of suite when a BAD_SETUP occurs.')


class ProjectTestBase(Suite):
    """This class acts as a base class for all tests within this project

    It is expected that helper functions that are useful for multiple test
    suites are placed here, they should be relevant to CleverSheep, other
    common test framework should be broken out into helper classes.

    TODO add extra common function examples such as a post fail or expecting
    events
    """

    def __init__(self):
        """Initialise variables used in the tests."""
        self.logger = logging.getLogger('ProjectTestBase')
        Suite.__init__(self)

    def setUp(self):
        """Core set up for all tests.

        This *must* be invoked; so any suite that over-rides setUp has to
        invoke its superclass implementation.

        """
        super(ProjectTestBase, self).setUp()

    def log_event_store(self, message, name=None, event_type=None):
        """Logs messages to the event store.

        :param message: The message to be logged.
        :param name: Name of the message.
        :param event_type: Type of the message.
        """
        eventStore.addEvent(TestEvent(message=message,
                                      name=name,
                                      type=event_type))

    def wait_for_event(self, max_wait, chop=True, **kwargs):
        """Returns a message from the event store. Waits for the specified
        time.

        :param max_wait: Time in seconds to wait for the expected message.
        :param chop: Whether to remove older events from the event store.
        :param kwargs: Parameters to be passed to the expect mechanism. It is
        useful to specify the name or type of the message to be returned.
        :return: Matching message from the message store.
        """
        def check():
            found = eventStore.find(expect, chop=chop)
            if found and not chop:
                eventStore.remove(found)
            return found

        def err():
            return ("Event matching %r did not occur within %.2f seconds"
                    % (kwargs, max_wait))

        expect = TestEvent(**kwargs)
        return self.control.expect(max_wait, check, err)


# Test filtering functions
def select_by_time(func, info):
    if not Tester.userOptions.max_time and not info.approx_exec_time:
        return True
    if info.approx_exec_time > Tester.userOptions.max_time:
        return False
    return True


def filter_test_failures(test, info):
    """Filter for tests marked as failing.

    Depending on the command line options this will select:

    - only tests that do not fail.
    - only tests that do fail.

    :Parameters:
      test
        The test object, as defined by CleverSheep.
      info
        The test information associated with the test.

    :Return:
        ``True`` if the test *is not* marked as failing.
    """
    # The --only-fatal option means only tests marked as 'testDies' should
    # be run.
    if Tester.userOptions.only_fatal and info.testFatal:
        print >> sys.__stdout__, "only-fatal"
        return False

    # The --only-failing option means only tests marked as 'testFails' should
    # be run.
    if Tester.userOptions.only_failing and info.testFails:
        print >> sys.__stdout__, "only-failing"
        return False

    # The --ignore-failing option means only tests not marked as 'testFails'
    # should be run.
    if Tester.userOptions.ignore_failing and info.testFails:
        return False

    # Add other command line options here.

    return True


def filter_todo(test, info):
    """Filter for tests marked as ``todo``.

    Depending on the command line options this will select:

    - only tests that are marked ``todo`` (--only-todo).
    - only tests that are not marked ``todo`` (--ignore-todo).

    :Parameters:
      test
        The test object, as defined by CleverSheep.
      info
        The test information associated with the test.

    :Return:
        ``True`` if the test *is not* marked as ``todo``.
    """
    # The --only-todo option means only tests marked as ``todo`` should
    # be run.
    if Tester.userOptions.only_todo:
        if not info.cs_flags.get("todo", False):
            return
    # The --ignore-todo option means only tests not marked as ``todo``
    # should be run.
    if Tester.userOptions.ignore_todo:
        if not info.cs_flags.get("todo", True):
            return

# Add the test filters to the framework.
Tester.addTestFilter(select_by_time)
Tester.addTestFilter(filter_test_failures)
Tester.addTestFilter(filter_todo)
