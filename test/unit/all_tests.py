#!/usr/bin/env python
"""Run all unit tests for the project"""
__docformat__ = "restructuredtext"

from CleverSheep.Test import Tester


if __name__ == "__main__":
    Tester.runTree()
