#!/usr/bin/env python
"""Example of unit tests."""

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule
from CleverSheep.Test.Tester import failUnlessEqual


class ExampleTestSuite(Suite):
    """Placeholder test file until there is something to test"""

    @test
    def test_example(self):
        """Test example"""
        # Arrange
        a = 2
        b = 4

        # Act
        c = a * a

        # Assert
        failUnlessEqual(b,c)


if __name__ == "__main__":
    runModule()