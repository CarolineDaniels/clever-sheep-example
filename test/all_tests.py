#!/usr/bin/env python
"""Run all tests for the project"""

from CleverSheep.Test import Tester


if __name__ == "__main__":
    Tester.runTree()
