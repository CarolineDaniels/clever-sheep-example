#!/usr/bin/env python
"""A very simple TCP server. This is the code that will be tested by the
CleverSheep example tests.

To test the TCP server, first start the server running:
<code>python simple_server.py</code>

Then in a separate window run the tests:
<code>./all_tests</code>

Command line parameters may be used to control which tests are run, and how
they are run, according to the test decorators on each test. See the individual
test files for more information.
"""
__docformat__ = "restructuredtext"

import logging

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )

class SimpleInt(object):
    """A very simple maths class representing an integer."""
    def __init__(self, start_at=0):
        """Initialise the class.

        :param start_at: initial value for the object.
        """
        self.logger = logging.getLogger('SimpleInt')
        self.value = start_at

    def increase(self, increase_by=0):
        """Increases the initial value by the integer parameter.

        :param increase_by: integer value to increase by
        """
        self.value += increase_by

    def exceeds(self, limit):
        """Checks if the value is greater than the limit supplied.

        :param exceeds: integer value of limit.
        :return: True if value is greater than limit, False otherwise.
        """
        return self.value > limit

    def is_positive(self):
        """Checks whether the object has a positive value.

        :return: True if the object has a positive value (>= 0).
        """
        return self.value >= 0

import SocketServer
from SocketServer import BaseRequestHandler, TCPServer


class MyTCPRequestHandler(BaseRequestHandler):
    """Simple custom request handler."""

    def __init__(self, request, client_address, server):
        self.logger = logging.getLogger('MyTCPRequestHandler')
        self.logger.info("Starting up...")
        BaseRequestHandler.__init__(self, request, client_address, server)

    def handle(self):
        """Handles all requests.

        Very simple request handling. The data that is received is sent back
        to the originator.
        """
        data = self.request.recv(1024).strip()
        self.logger.info('Received %s', data)
        self.request.send(data)


class MySocketServer (TCPServer):
    """Simple TCP server."""

    def __init__(self, server_address, handler_class=MyTCPRequestHandler):
        SocketServer.TCPServer.__init__(self, server_address, handler_class)
        self.logger = logging.getLogger('MySocketServer')

if __name__ == '__main__':

    address = ('localhost', 20009)
    server = MySocketServer(address, MyTCPRequestHandler)

    # find out the address and port so they can be displayed.
    ip, port = server.server_address
    logger = logging.getLogger('main')
    logger.info('Server on %s:%s', ip, port)

    # Run until the user presses CTRL-C
    server.serve_forever()
