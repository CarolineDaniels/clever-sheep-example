NOTE: This is still a work in progress, issue #19 in the main repo will cover
extending it to actually contain some useful examples.

# Introduction

This repo is intended to act as both documentation for and examples of the
CleverSheep library.

The online documentation which this repo compliments can be found at
https://lcaraccio.gitlab.io/clever-sheep/api/index.html

The issue tracker for the library can be found at https://gitlab.com/LCaraccio/clever-sheep
any issues with this project should be raised there.

# Using this example repo

This repo is intended to be fully usable, it may be checked out and assuming
the pre-requisites have been met the tests can be run to allow you to
try using the library.

The files themselves should contain documentation of what they are and what
they are for so if you see something in the tree open it up to find out more.

## Pre-requisites

TBC - at least the latest version of CleverSheep, may want to set up a CI
job for this repo

It may also want things like pep8 run on it, it is code after all.