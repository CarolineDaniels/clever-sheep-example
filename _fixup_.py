"""Perform pre test setup

This file will be loaded in before the tests start allowing you to perform
pre test one time setup such as adding folders to the path if required or
calling setup of libraries such as Django.

This file must not import CleverSheep as the library is still setting up when
this file is loaded in.
"""

print("Performing __fixup__")